# Coders Canteen

Aplikacja internetowa mająca na celu zebranie projektów tworzonych przez studentów ISSP w jednym miejscu i atrakcyjne zaprezentowanie ich szerszemu gronu odbiorców. Aplikacja składać będzie się z portfolio kierunku z podziałem na poszczególne cykle studiów. Podstrona każdego projektu zawierać będzie obszerny opis, stan oraz media związane z pracami studentów tak by jak najlepiej przybliżyć użytkownikowi działanie danego projektu.

* Aplikacja zostanie wykonana w oparciu o framework NUXT.js oraz platformę Firebase do przechowywania danych i obsługi zapytań.
* Za stronę wizualną odpowiadać będzie biblioteka Bootstrap.

### Planowany termin ukończenia projektu: 31 V 2018
