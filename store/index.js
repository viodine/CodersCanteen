import Vuex from 'vuex';

const createStore = () => {
    return new Vuex.Store({
        state: {
            loadedProjects: [],
            loadedAuthors: []
        },
        mutations: {
            setProjects(state, projects) {
                state.loadedProjects = projects;
            },
            setAuthors(state, authors) {
                state.loadedAuthors = authors;
            }
        },
        actions: {
            async nuxtServerInit(vuexContext, context) {
                let projects = await context.app.$axios.$get("https://coderscanteen.firebaseio.com/projects.json")
                    .then(data => {
                        let projectsArray = [];
                        for (let key in data) {
                            projectsArray.push({ ...data[key], id: key });
                        }
                        vuexContext.commit("setProjects", projectsArray);
                    })
                    .catch(e => context.error(e));
                let authors = await context.app.$axios.$get("https://coderscanteen.firebaseio.com/authors.json")
                .then(data => {
                    let authorsArray = [];
                    for (let key in data) {
                        authorsArray.push({ ...data[key], id: key });
                    }
                    vuexContext.commit("setAuthors", authorsArray);
                })
                .catch(e => context.error(e));
                return {
                    projects,
                    authors
                }
            },
            setProjects(vuexContext, projects) {
                vuexContext.commit('setProjects', projects);
            },
            setAuthors(vuexcontext, authors) {
                vuexContext.commit('setAuthors', authors);
            }
        },
        getters: {
            loadedProjects(state) {
                return state.loadedProjects;
            },
            loadedAuthors(state) {
                return state.loadedAuthors;
            }
        },
    })
}

export default createStore;